import api from '../../api/api'
import { topicList } from '../../api/conf'
Page({
  data: {
    questionList:[],
    page:1,
    reTurn:false
  },
  onLoad: function (options) {
    this.getTopicList()
  },
  onReady: function () {

  },
  onShow: function () {

  },
  lower:function(){
    if (this.data.reTurn){
      return
    }
    this.setData({
      page: this.data.page + 1
    })
    this.getTopicList()
  },
  getTopicList:function(){
    wx.showLoading({
      title: '加载中',
    })
    var that = this
    if (this.data.reTurn) {
      return
    }
    api.get(topicList, {
      page: that.data.page
    }).then(res => {
      let arr = []
      //过滤notice
      for (let i = 0; i < res.Data.Topics.length;i++){
        if (res.Data.Topics[i].SystemName !='notice'){
          arr.push(res.Data.Topics[i])
        }
      }
      that.setData({
        questionList: that.data.questionList.concat(arr)
      })
      if (res.Data.Topics.length < 12) {
        that.setData({
          reTurn: true
        })
      }
      wx.hideLoading()
    })
  },
  goquestion:function(e){
    wx.navigateTo({
      url: "/pages/question/question?name=" + e.currentTarget.dataset.name,
    })
  }
})