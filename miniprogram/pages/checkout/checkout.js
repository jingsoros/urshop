const app = getApp()
var qqMap = require('../../libs/qqmap-wx-jssdk.min.js');
var qmapSDK;
import api from '../../api/api'
import {
  getcheckout
} from '../../api/conf'
import {
  submitcheckout
} from '../../api/conf'
import {
  paymentweixinwap
} from '../../api/conf'
import {
  addressList
} from '../../api/conf'
import {
  getcustomer
} from '../../api/conf'
Page({
  data: {
    addressInfo: {},
    phone: '',
    name: '',
    weixinPay: true,
    checkoutInfo: {},
    remark: '',
    phoneChange: true,
    phoneFocus: false,
    nameChange: true,
    nameFocus: false,
    location: '',
    money: null,
    stop: false,
    AdresList: null,
    showTrueAgain: false,
    idcar: '',
    customerId: '',
    proid: '',
    ptid: ''
  },
  onLoad: function(options) {
    // if (!wx.getStorageSync('adress')){
    //   this.clickNameShow()
    //   this.clickChangeShow()
    // }
    // if (wx.getStorageSync('name')){
    //   that.setData({
    //     phone: wx.getStorageSync('phone'),
    //     name: wx.getStorageSync('name')
    //   })
    // }
    // that.setData({
    //   AdresList: app.globalData.currentAdress
    // })
    if (options.customerid) {
      this.setData({
        customerId: options.customerid,
        proid: options.proid,
        ptid: options.ptid
      })
    }
    this.getCustomer()
  },
  onReady: function() {
    // this.getLocation()
  },
  onShow: function() {
    this.getchckouts()
  },
  getchckouts: function() {
    var that = this
    wx.showLoading({
      title: '加载中',
      icon: 'none'
    })
    api.get(getcheckout, {}).then(res => {
      let num = res.Data.OrderTotal
      let price = parseFloat(num.substr(1))
      that.setData({
        checkoutInfo: res.Data,
        money: price,
        addressInfo: res.Data.Address
      })
      wx.hideLoading()
    })
  },
  weixinpayfalse: function() {
    this.setData({
      weixinPay: false
    })
  },
  weixinpaytrue: function() {
    this.setData({
      weixinPay: true
    })
  },
  remarkSub: function(e) {
    this.setData({
      remark: e.detail.value
    })
  },
  orderPay: function() {
    if (this.data.addressInfo.Name == '') {
      wx.showToast({
        title: '收货人必填',
        icon: 'none'
      })
    } else if (this.data.addressInfo.Phone == '') {
      wx.showToast({
        title: '手机号必填',
        icon: 'none'
      })
    }
    else {
      this.setData({
        showTrueAgain: true
      })
    }
  },
  //确定支付
  truePay: function() {
    if (this.data.weixinPay) {
      this.weixinPay()
    } else {
      this.BalancePay()
    }
    wx.setStorageSync('name', this.data.name)
    wx.setStorageSync('phone', this.data.phone)
    wx.setStorageSync('adress', this.data.AdresList)
  },
  //添加历史，暂时无用
  addHistory: function() {
    let arr = []
    let panduan = false
    let Adres = this.data.AdresList
    if (wx.getStorageSync('historyAdress')) {
      arr = wx.getStorageSync('historyAdress')
    }
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].Id == Adres.Id) {
        panduan = true
      }
    }
    if (panduan) {
      return
    }
    arr.push(Adres)
    wx.setStorageSync('historyAdress', arr)
  },
  //现金支付
  BalancePay: function() {
    var that = this
    if (this.data.stop) {
      return
    }
    this.setData({
      stop: true
    })
    wx.showLoading({
      title: '正在支付',
      icon: 'none'
    })
    api.post(submitcheckout, {
      AddressId: that.data.addressInfo.Id,
      Balance: that.data.money,
      Remark: that.data.remark,
      PTCustomerId: that.data.customerId || 0
    }).then(res => {
      that.setData({
        stop: false
      })
      let infoObj = {
        payShow: true,
        orderId: res.Data.OrderId,
        time: res.Data.CreateTime,
        price: res.Data.OrderPrice
      }
      var info = JSON.stringify(infoObj)
      wx.hideLoading()
      wx.showToast({
        title: '支付成功',
      })
      setTimeout(function() {
          wx.redirectTo({
            url: '/pages/payFinish/payFinish?info=' + info
          })
      }, 1500)
    }).catch(err => {
      that.setData({
        stop: false
      })
      wx.hideLoading()
      wx.showModal({
        title: '余额不足',
        content: '是否前往充值页面？',
        showCancel: true,
        success: function(res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/recharge/recharge',
            })
          } else {

          }
        }
      })
    })
  },
  weixinPay: function() {
    var that = this
    if (this.data.stop) {
      return
    }
    this.setData({
      stop: true
    })
    wx.showLoading({
      title: '正在支付',
      icon: 'none'
    })
    api.post(submitcheckout, {
      AddressId: that.data.addressInfo.Id,
      // PickupPointId: that.data.AdresList.Id,
      PaymentMethod: 'Payments.WeixinOpen',
      Remark: '备注: ' + that.data.remark,
      PTCustomerId: that.data.customerId || 0
    }).then(res => {
      that.pay(res)
    })
  },
  clickChangeShow: function() {
    this.setData({
      phoneChange: false,
      phone: '',
      phoneFocus: true
    })
  },
  clickChangeFinish: function() {
    if (!(/^1[34578]\d{9}$/.test(this.data.phone))) {
      wx.showToast({
        title: '手机号格式有误',
        icon: 'none'
      })
      return
    }
    this.setData({
      phoneChange: true
    })
  },
  changephonenumber: function(e) {
    this.setData({
      phone: e.detail.value
    })
  },
  clickNameShow: function() {
    this.setData({
      nameChange: false,
      name: '',
      nameFocus: true
    })
  },
  clickNameFinish: function() {
    this.setData({
      nameChange: true
    })
  },
  changeNamenumber: function(e) {
    this.setData({
      name: e.detail.value
    })
  },
  getLocation: function(e) {
    var that = this;
    //初始化
    qmapSDK = new qqMap({
      key: 'VTNBZ-OJEW4-LMYUW-DHOHT-R3D65-OWFCY'
    })
    //获取地址
    wx.getLocation({
      success: function(res) {
        type: 'wgs84'
        qmapSDK.reverseGeocoder({
          location: {
            latitude: res.latitude,
            longitude: res.longitude
          },
          success: function(e) {
            var location = e.result.location;
            that.setData({
              location: location
            })
          }
        })
      },
    })
  },
  pay: function(res) {
    var that = this
    api.get(paymentweixinwap, {
      orderId: res.Data.OrderId,
      paytype: 'Order'
    }).then(res => {
      that.setData({
        stop: false
      })
      let infoObj = {
        payShow: true,
        orderId: res.Data.OrderId,
        time: res.Data.CreateTime,
        price: res.Data.OrderPrice
      }
      var info = JSON.stringify(infoObj)
      wx.hideLoading()
      if (res.Code == 200) {
        let rep = res.Data.KeyValue
        wx.requestPayment({
          timeStamp: rep.timeStamp,
          nonceStr: rep.nonceStr,
          package: rep.package,
          signType: rep.signType,
          paySign: rep.paySign,
          success(res) {
            wx.showToast({
              title: '支付成功！',
              icon: 'success'
            })
            wx.redirectTo({
              url: '/pages/payFinish/payFinish?info=' + info
            })
          },
          fail(err) {
            wx.showToast({
              title: '支付失败！',
              icon: 'success'
            })
            setTimeout(function() {
              wx.switchTab({
                url: '/pages/my/my',
              })
            }, 1500)
          }
        })
      }
    })
  },
  closeMask: function() {
    this.setData({
      showTrueAgain: false
    })
  },
  getCustomer: function() {
    var that = this
    api.get(getcustomer).then(res => {
      that.setData({
        idcar: res.Data.Fields[2].DefaultValue
      })
    })
  },
})