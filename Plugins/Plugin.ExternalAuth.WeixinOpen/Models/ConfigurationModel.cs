﻿using Urs.Framework;
using Urs.Framework.Mvc;

namespace ExternalAuth.WeixinOpen.Models
{
    public class ConfigurationModel : BaseModel
    {
        [UrsDisplayName("Plugins.ExternalAuth.WeixinOpen.AppId")]
        public string AppId { get; set; }

        [UrsDisplayName("Plugins.ExternalAuth.WeixinOpen.AppSecret")]
        public string AppSecret { get; set; }

        [UrsDisplayName("Plugins.ExternalAuth.WeixinOpen.CallBack")]
        public string CallBack { get; set; }

        [UrsDisplayName("Plugins.ExternalAuth.WeixinOpen.ApiCallBack")]
        public string ApiCallBack { get; set; }
    }
}