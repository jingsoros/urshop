﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Urs.Framework.Mvc.Routes;

namespace Urs.Plugin.Weixin.MiniSDK.Infrastructure
{
    public partial class RouteProvider : IRouteProvider
    {
        /// <summary>
        /// Register routes
        /// </summary>
        /// <param name="routeBuilder">Route builder</param>
        public void RegisterRoutes(IRouteBuilder routes)
        {
            routes.MapRoute("Plugin.Weixin.MiniSDK.Configure",
               "Plugins/WeixinMiniSDK/Configure",
               new { controller = "WeixinMiniSDK", action = "Configure" });

            routes.MapRoute("Plugin.Weixin.MiniSDK.Api",
                 "Plugins/WeixinMiniSDK/Api",
                 new { controller = "WeixinMiniSDK", action = "Api" }
            );
        }

        /// <summary>
        /// Gets a priority of route provider
        /// </summary>
        public int Priority
        {
            get { return 0; }
        }
    }
}