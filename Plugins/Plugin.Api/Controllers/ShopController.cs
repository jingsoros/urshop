﻿using Microsoft.AspNetCore.Mvc;
using Plugin.Api.Models.Catalog;
using System.Collections.Generic;
using System.Threading.Tasks;
using Urs.Core;
using Urs.Data.Domain.Configuration;
using Urs.Framework.Controllers;
using Urs.Services.Media;
using Urs.Services.Plugins;
using Urs.Services.Stores;

namespace Plugin.Api.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/shop")]
    [ApiController]
    public partial class ShopController : BaseApiController
    {
        #region Fields

        private readonly IWorkContext _workContext;
        private readonly IPictureService _pictureService;
        private readonly IWebHelper _webHelper;
        private readonly IPluginFinder _pluginFinder;
        private readonly ShippingSettings _shippingSettings;
        private readonly IShopService _shopService;

        #endregion
        /// <summary>
        /// 构造器
        /// </summary>
        #region Constructors

        public ShopController(
            IWorkContext workContext, IPictureService pictureService,
            IWebHelper webHelper, IPluginFinder pluginFinder,
            ShippingSettings shippingSettings,
            IShopService shopService)
        {
            this._pluginFinder = pluginFinder;
            this._workContext = workContext;
            this._pictureService = pictureService;
            this._webHelper = webHelper;
            this._shippingSettings = shippingSettings;
            this._shopService = shopService;
        }

        #endregion

        #region Utilities
        /// <summary>
        /// 自提地址列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("shoplist")]
        public async Task<ApiResponse<List<MoShop>>> List()
        {
            var response = await Task.Run(() =>
            {
                var shops = _shopService.GetAllShop();
                var list = new List<MoShop>();
                foreach (var item in shops)
                    list.Add(new MoShop()
                    {
                        Id = item.Id.ToString(),
                        Name = item.Name,
                        Code=item.Code,
                        Remark = item.Remark,
                        Latitude = item.Longitude,
                        Longitude = item.Latitude,
                    });
                return ApiResponse<List<MoShop>>.Success(list);
            });
            return response;
        }
        #endregion

    }
}
