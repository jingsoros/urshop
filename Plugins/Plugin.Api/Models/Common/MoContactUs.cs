﻿using Urs.Framework.Mvc;

namespace Plugin.Api.Models.Common
{
    public partial class MoContactUs : BaseModel
    {
        /// <summary>
        /// email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 咨询内容
        /// </summary>
        public string Enquiry { get; set; }
        /// <summary>
        /// 用户
        /// </summary>
        public string FullName { get; set; }

        public bool SuccessfullySent { get; set; }
        public string Result { get; set; }

        public bool DisplayCaptcha { get; set; }
    }
}