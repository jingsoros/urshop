﻿namespace Urs.Admin.Models.Settings
{
    /// <summary>
    /// 模板
    /// </summary>
    public class MoTemplateSettings
    {
        /// <summary>
        /// Picture
        /// </summary>
        public int Picture1Id { get; set; }
        /// <summary>
        /// 图片Url
        /// </summary>
        public string Picture1Url { get; set; }
        /// <summary>
        /// Text
        /// </summary>
        public string Text1 { get; set; }
        /// <summary>
        /// SubText
        /// </summary>
        public string SubText1 { get; set; }
        /// <summary>
        /// Time
        /// </summary>
        public string Time1 { get; set; }
        /// <summary>
        /// Ext
        /// </summary>
        public string Ext1 { get; set; }
        /// <summary>
        /// AltText
        /// </summary>
        public string AltText1 { get; set; }

        /// <summary>
        /// Picture
        /// </summary>
        public int Picture2Id { get; set; }
        /// <summary>
        /// 图片Url
        /// </summary>
        public string Picture2Url { get; set; }
        /// <summary>
        /// Text
        /// </summary>
        public string Text2 { get; set; }
        /// <summary>
        /// SubText
        /// </summary>
        public string SubText2 { get; set; }
        /// <summary>
        /// Time
        /// </summary>
        public string Time2 { get; set; }
        /// <summary>
        /// Ext
        /// </summary>
        public string Ext2 { get; set; }
        /// <summary>
        /// AltText
        /// </summary>
        public string AltText2 { get; set; }


        /// <summary>
        /// Picture
        /// </summary>
        public int Picture3Id { get; set; }
        /// <summary>
        /// 图片Url
        /// </summary>
        public string Picture3Url { get; set; }
        /// <summary>
        /// Text
        /// </summary>
        public string Text3 { get; set; }
        /// <summary>
        /// SubText
        /// </summary>
        public string SubText3 { get; set; }
        /// <summary>
        /// Time
        /// </summary>
        public string Time3 { get; set; }
        /// <summary>
        /// Ext
        /// </summary>
        public string Ext3 { get; set; }
        /// <summary>
        /// AltText
        /// </summary>
        public string AltText3 { get; set; }
        /// <summary>
        /// 默认获取哪个？
        /// </summary>
        public int Default { get; set; }
        /// <summary>
        /// 启用
        /// </summary>
        public bool Enabled { get; set; }
        /// <summary>
        /// 天气图片
        /// </summary>
        public int TianQiId1 { get; set; }
        public int TianQiId2 { get; set; }
        public int TianQiId3 { get; set; }
        public int TianQiId4 { get; set; }
        public int TianQiId5 { get; set; }
        public int TianQiId6 { get; set; }
        public int TianQiId7 { get; set; }

        public string TianQiId1Url { get; set; }
        public string TianQiId2Url { get; set; }
        public string TianQiId3Url { get; set; }
        public string TianQiId4Url { get; set; }
        public string TianQiId5Url { get; set; }
        public string TianQiId6Url { get; set; }
        public string TianQiId7Url { get; set; }

    }
}