﻿using System.Collections.Generic;
using Urs.Data.Domain.Common;

namespace Plugin.Api.Models.Common
{
    /// <summary>
    /// 提交表单
    /// </summary>
    public class MoFormData
    {
        /// <summary>
        /// Id编号
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string Phone { get; set; }
    }
}