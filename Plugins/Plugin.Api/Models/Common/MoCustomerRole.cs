﻿namespace Urs.Plugin.Api.Models.Common
{
    /// <summary>
    /// 角色
    /// </summary>
    public class MoUserRole
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 系统名称
        /// </summary>
        public string SystemName { get; set; }
    }
}
