﻿namespace Plugin.Api.Models.Account
{
    /// <summary>
    /// 修改密码
    /// </summary>
    public partial class MoChangePassword
    {
        /// <summary>
        /// 用户标识Guid
        /// </summary>
        public string Guid { get; set; }
        /// <summary>
        /// 旧密码
        /// </summary>
        public string OldPassword { get; set; }
        /// <summary>
        /// 新密码
        /// </summary>
        public string NewPassword { get; set; }

    }
}