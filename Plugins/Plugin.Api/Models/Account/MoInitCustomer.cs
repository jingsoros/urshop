﻿using System;

namespace Plugin.Api.Models.Account
{
    /// <summary>
    /// 初始化用户
    /// </summary>
    public partial class MoInitUser
    {
        /// <summary>
        /// 用户标记(Guid)
        /// </summary>
        public string Guid { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}