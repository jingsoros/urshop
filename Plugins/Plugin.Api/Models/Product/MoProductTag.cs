﻿namespace Plugin.Api.Models.Goods
{
    /// <summary>
    /// 标签
    /// </summary>
    public partial class MoGoodsTag
    {
        /// <summary>
        /// 标签Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        ///  Url
        /// </summary>
        public string SeName { get; set; }
    }
}