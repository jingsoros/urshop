﻿using System;
using Urs.Services.Tasks;

namespace Urs.Services.Orders
{
    public partial class DeleteUnpaidOrderTask : IScheduleTask
    {
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderService _orderService;

        public DeleteUnpaidOrderTask(IOrderProcessingService orderProcessingService,
            IOrderService orderService)
        {
            this._orderProcessingService = orderProcessingService;
            this._orderService = orderService;
        }

        public void Execute()
        {
            _orderProcessingService.CancelUnpaidOrder();
        }
    }
}
