

namespace Urs.Services.Authentication.External
{
    public struct RegistrationDetails
    {
        public RegistrationDetails(OpenAuthParameters parameters)
            : this()
        {
            var claim = parameters.UserClaim;
            Nickname = claim.Nickname;
            ImageUrl = claim.HeadImgUrl;
        }
        public string UserName { get; set; }
        public string Phone { get; set; }
        public string Nickname { get; set; }
        public string ImageUrl { get; set; }
    }
}