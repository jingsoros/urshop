using System.Collections.Generic;

namespace Urs.Services.Configuration
{
    public partial interface IWidgetService
    {
        IList<IWidgetPlugin> LoadActiveWidgets();

        IList<IWidgetPlugin> LoadActiveWidgetsByWidgetZone(string widgetZone);
        IWidgetPlugin LoadWidgetBySystemName(string systemName);

        IList<IWidgetPlugin> LoadAllWidgets();
    }
}
