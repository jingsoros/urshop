using System.Collections.Generic;
using Urs.Data.Domain.Directory;

namespace Urs.Services.Directory
{
    public partial interface IMeasureService
    {
        void DeleteMeasureDimension(MeasureDimension measureDimension);

        MeasureDimension GetMeasureDimensionById(int measureDimensionId);

        MeasureDimension GetMeasureDimensionBySystemKeyword(string systemKeyword);

        IList<MeasureDimension> GetAllMeasureDimensions();

        void InsertMeasureDimension(MeasureDimension measure);

        void UpdateMeasureDimension(MeasureDimension measure);

        decimal ConvertDimension(decimal quantity,
            MeasureDimension sourceMeasureDimension, MeasureDimension targetMeasureDimension, bool round = true);

        decimal ConvertToPrimaryMeasureDimension(decimal quantity,
            MeasureDimension sourceMeasureDimension);

        decimal ConvertFromPrimaryMeasureDimension(decimal quantity,
            MeasureDimension targetMeasureDimension);
        

        void DeleteMeasureWeight(MeasureWeight measureWeight);

        MeasureWeight GetMeasureWeightById(int measureWeightId);

        MeasureWeight GetMeasureWeightBySystemKeyword(string systemKeyword);

        IList<MeasureWeight> GetAllMeasureWeights();

        void InsertMeasureWeight(MeasureWeight measure);

        void UpdateMeasureWeight(MeasureWeight measure);

        decimal ConvertWeight(decimal quantity,
            MeasureWeight sourceMeasureWeight, MeasureWeight targetMeasureWeight, bool round = true);

        decimal ConvertToPrimaryMeasureWeight(decimal quantity, MeasureWeight sourceMeasureWeight);

        decimal ConvertFromPrimaryMeasureWeight(decimal quantity,
            MeasureWeight targetMeasureWeight);
    }
}