using System.Collections.Generic;
using Urs.Data.Domain.Stores;

namespace Urs.Services.Stores
{
    public partial interface IGoodsTagService
    {
        void DeleteGoodsTag(GoodsTag goodsTag);

        IList<GoodsTag> GetAllGoodsTags(bool showHidden = false);

        GoodsTag GetGoodsTagById(int goodsTagId);
        
        GoodsTag GetGoodsTagByName(string name);

        void InsertGoodsTag(GoodsTag goodsTag);

        void UpdateGoodsTag(GoodsTag goodsTag);

        void UpdateGoodsTagTotals(GoodsTag goodsTag);
    }
}
