using System;
using System.Collections.Generic;
using Urs.Data.Domain.Common;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Directory;
using Urs.Data.Domain.Orders;

namespace Urs.Services.Shipping
{
    public partial class GetShippingOptionRequest
    {
        public GetShippingOptionRequest()
        {
            this.Items = new List<ShoppingCartItem>();
        }
        public virtual IList<ShoppingCartItem> Items { get; set; }

        public virtual Address ShippingAddress { get; set; }

        public virtual string ZipPostalCodeFrom { get; set; }
    }
}
