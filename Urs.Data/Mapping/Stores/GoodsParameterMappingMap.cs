
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class GoodsParameterMappingMap : UrsEntityTypeConfiguration<GoodsParameter>
    {
        public override void Configure(EntityTypeBuilder<GoodsParameter> builder)
        {
            builder.ToTable(nameof(GoodsParameter));
            builder.HasKey(sa => sa.Id);
            builder.Property(sa => sa.Name).IsRequired();
            base.Configure(builder);
        }
    }
}