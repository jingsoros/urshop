using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Agents;

namespace Urs.Data.Mapping.Agents
{
    public partial class AgentBonusMap : UrsEntityTypeConfiguration<AgentBonus>
    {
        public override void Configure(EntityTypeBuilder<AgentBonus> builder)
        {
            builder.ToTable(nameof(AgentBonus));
            builder.HasKey(x => x.Id);

            base.Configure(builder);
        }
    }
}