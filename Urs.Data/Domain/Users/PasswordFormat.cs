﻿using Urs.Core;

namespace Urs.Data.Domain.Users 
{ 
    public enum PasswordFormat : int
    {
        Clear = 0,
        Hashed = 1,
        Encrypted = 2
    }
}
