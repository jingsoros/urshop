﻿using System.Runtime.Serialization;

namespace Urs.Core.Data
{
    /// <summary>
    /// Represents data provider type enumeration
    /// </summary>
    public enum DataProviderType
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [EnumMember(Value = "")]
        Unknown,


        [EnumMember(Value = "mysql")]
        Mysql
    }
}