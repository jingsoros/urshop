﻿using Microsoft.AspNetCore.Mvc;

namespace Urs.Web.Controllers
{
    /// <summary>
    /// www.urselect.com 优社电商
    /// </summary>
    public partial class HomeController : Controller
    {
        /// <summary>
        ///  首页
        /// </summary>
        public virtual IActionResult Index()
        {
            return Redirect("/admin/login");
        }
    }
}
