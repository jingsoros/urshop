﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Urs.Admin.Models.Banners;
using Urs.Core;
using Urs.Data.Domain.Banners;
using Urs.Data.Domain.Configuration;
using Urs.Services.Banners;
using Urs.Services.Localization;
using Urs.Services.Logging;
using Urs.Services.Media;
using Urs.Services.Security;
using Urs.Framework.Controllers;
using Urs.Framework.Extensions;
using Urs.Framework.Kendoui;

namespace Urs.Admin.Controllers
{
    [AdminAuthorize]
    public partial class BannerController : BaseAdminController
    {
        #region Fields

        private readonly IBannerService _bannerService;
        private readonly IPictureService _pictureService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly IActivityLogService _activityLogService;
        private readonly IPermissionService _permissionService;
        private readonly AdminAreaSettings _adminAreaSettings;

        #endregion

        #region Constructors
        public BannerController(
            IBannerService bannerService,
            IPictureService pictureService,
            ILocalizationService localizationService,
            IWorkContext workContext,
            IActivityLogService activityLogService,
            IPermissionService permissionService,
            AdminAreaSettings adminAreaSettings)
        {
            this._bannerService = bannerService;
            this._pictureService = pictureService;
            this._localizationService = localizationService;
            this._workContext = workContext;
            this._activityLogService = activityLogService;
            this._permissionService = permissionService;
            this._adminAreaSettings = adminAreaSettings;
        }

        #endregion

        #region List

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBanner))
                return HttpUnauthorized();

            var model = new BannerListModel();

            return View(model);
        }

        [HttpPost]
        public IActionResult List(PageRequest command, BannerListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBanner))
                return HttpUnauthorized();

            var banner = _bannerService.GetAll(command.Page - 1, command.Limit);
            var result = new ResponseResult
            {
                data = banner.Select(x => x.ToModel<BannerModel>()),
                count = banner.TotalCount
            };
            return Json(result);
        }

        public IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBanner))
                return HttpUnauthorized();

            var banner = _bannerService.GetById(id);
            if (banner == null)
            {
                var models = new BannerModel();
                return View(models);
            }
            var model = banner.ToModel<BannerModel>();
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(BannerModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBanner))
                return HttpUnauthorized();

            var banner = _bannerService.GetById(model.Id);
            if (banner == null)
            {
                var banners = model.ToEntity<Banner>();
                _bannerService.InsertBannerZone(banners);
            }
            else
            {
                if (ModelState.IsValid)
                {
                    banner = model.ToEntity(banner);
                    _bannerService.UpdateBannerZone(banner);
                }
            }
            return Json(new { success = 1 });
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBanner))
                return HttpUnauthorized();

            var banner = _bannerService.GetById(id);
            if (banner == null)
                return Json(new { error = 1 });

            _bannerService.DeleteBannerZone(banner);
            return Json(new { success = 1 });
        }
        #endregion

        #region Goods spec

        public IActionResult Banner(int id)
        {
            ViewBag.BannerId = id;
            return View();
        }

        [HttpPost]
        public IActionResult BannerItem(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBanner))
                return HttpUnauthorized();

            var bannerItemList = _bannerService.GetItemsById(id);
            var bannerItemListModel = bannerItemList
                .Select(x =>
                {
                    var pvaModel = x.ToModel<BannerItemModel>();
                    pvaModel.PictureUrl = _pictureService.GetPictureUrl(x.PictureId, 200);
                    return pvaModel;
                })
                .ToList();

            var model = new ResponseResult
            {
                data = bannerItemListModel,
                count = bannerItemListModel.Count
            };
            return Json(model);
        }

        public IActionResult ItemUpdate(int id, int zoneId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBanner))
                return HttpUnauthorized();

            var model = new BannerItemModel();
            var pva = _bannerService.GetBannerItemById(id);

            if (pva != null)
                model = pva.ToModel<BannerItemModel>();

            model.BannerZoneId = zoneId;
            return View(model);
        }

        [HttpPost]
        public IActionResult ItemUpdate(BannerItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBanner))
                return HttpUnauthorized();

            var pva = _bannerService.GetBannerItemById(model.Id);
            if (pva == null)
            {
                pva = new BannerItem();
                pva.Title = model.Title;
                pva.Url = model.Url;
                pva.BannerId = model.BannerZoneId;
                pva.SubTitle = model.SubTitle;
                pva.PictureId = model.PictureId;
                pva.DisplayOrder = model.DisplayOrder;
                _bannerService.InsertItem(pva);
            }
            else
            {
                pva.Title = model.Title;
                pva.Url = model.Url;
                pva.SubTitle = model.SubTitle;
                pva.PictureId = model.PictureId;
                pva.DisplayOrder = model.DisplayOrder;
                _bannerService.UpdateItem(pva);
            }
            return Json(new { success = 1 });
        }

        [HttpPost]
        public IActionResult ItemDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBanner))
                return HttpUnauthorized();

            var pva = _bannerService.GetBannerItemById(id);
            if (pva == null)
                return Json(new { error = 1 });

            _bannerService.DeleteItem(pva);

            return Json(new { success = 1 });
        }

        #endregion


    }
}
