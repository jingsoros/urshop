﻿using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using Urs.Admin.Validators.Settings;
using Urs.Framework;
using Urs.Framework.Mvc;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Settings
{
    [Validator(typeof(SettingValidator))]
    public partial class SettingModel : BaseEntityModel
    {
        [UrsDisplayName("Admin.Configuration.Settings.AllSettings.Fields.Name")]
        
        public string Name { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.AllSettings.Fields.Value")]
        
        public string Value { get; set; }
    }
}