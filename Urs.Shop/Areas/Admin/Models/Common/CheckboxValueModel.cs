﻿namespace Urs.Admin.Models.Common
{
    public class CheckboxValueModel
    {
        public int Id { get; set; }

        public string Value { get; set; }

        public bool Checked { get; set; }
    }
}
