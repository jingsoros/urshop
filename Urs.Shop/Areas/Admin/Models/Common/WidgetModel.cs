﻿using Microsoft.AspNetCore.Routing;
using Urs.Framework;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Common
{
    public partial class WidgetModel : BaseModel, IPluginModel
    {
        [UrsDisplayName("Admin.ContentManagement.Widgets.Fields.FriendlyName")]
        public string FriendlyName { get; set; }

        [UrsDisplayName("Admin.ContentManagement.Widgets.Fields.SystemName")]

        public string SystemName { get; set; }

        [UrsDisplayName("Admin.ContentManagement.Widgets.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        [UrsDisplayName("Admin.ContentManagement.Widgets.Fields.IsActive")]
        public bool IsActive { get; set; }


        public string ConfigurationActionName { get; set; }
        public string ConfigurationControllerName { get; set; }
        public RouteValueDictionary ConfigurationRouteValues { get; set; }
        public string ConfigurationUrl { get; set; }
        public string LogoUrl { get; set; }
    }
}