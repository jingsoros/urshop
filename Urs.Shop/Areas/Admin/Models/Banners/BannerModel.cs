﻿using System.ComponentModel;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Banners
{
    public partial class BannerModel : BaseEntityModel
    {
        [DisplayName("名称")]
        public virtual string Name { get; set; }
        [DisplayName("样式名称")]
        public virtual string ClassName { get; set; }
        [DisplayName("空间名称")]
        public virtual string ZoneName { get; set; }
        [DisplayName("模板名称(非必填)")]
        public virtual string TemplateName { get; set; }
    }
}