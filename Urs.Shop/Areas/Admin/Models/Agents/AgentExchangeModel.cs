﻿using System;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Agents
{
    public partial class AgentExchangeModel : BaseEntityModel
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 取现记录
        /// </summary>
        public decimal ExchangFee { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 转账账户 如：支付宝 846033233@qq.com 谢先生
        /// </summary>
        public string WeixinAccount { get; set; }
        /// <summary>
        /// 是否已转账
        /// </summary>
        public bool IsTransfer { get; set; }
        /// <summary>
        /// 转账时间
        /// </summary>
        public DateTime? TransferTime { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 转账流水
        /// </summary>
        public string SerialNumber { get; set; }
        /// <summary>
        /// 是否删除
        /// </summary>
        public bool Deleted { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}