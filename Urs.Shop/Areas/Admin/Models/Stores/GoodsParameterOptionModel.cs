﻿using FluentValidation.Attributes;
using Urs.Admin.Validators.Stores;
using Urs.Framework;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Stores
{
    [Validator(typeof(GoodsParameterOptionValidator))]
    public partial class GoodsParameterOptionModel : BaseEntityModel
    {
        public int GoodsParameterId { get; set; }

        [UrsDisplayName("Admin.Store.GoodsParameters.Options.Fields.Name")]
        
        public string Name { get; set; }

        [UrsDisplayName("Admin.Store.GoodsParameters.Options.Fields.DisplayOrder")]
        public int DisplayOrder {get;set;}

    }
}