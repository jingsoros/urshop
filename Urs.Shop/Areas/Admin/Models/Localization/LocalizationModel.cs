﻿using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using Urs.Admin.Validators.Localization;
using Urs.Framework;
using Urs.Framework.Mvc;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Localization
{
    [Validator(typeof(LocalizationValidator))]
    public partial class LocalizationModel : BaseEntityModel
    {
        [UrsDisplayName("Admin.Configuration.Languages.Resources.Fields.Name")]
        
        public string Name { get; set; }

        [UrsDisplayName("Admin.Configuration.Languages.Resources.Fields.Value")]
        
        public string Value { get; set; }

    }
}